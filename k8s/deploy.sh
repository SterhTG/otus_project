#!/bin/bash
install_base() {
    #Deloy base services to kubernetes cluster
    kubectl apply -f bootstrap
    return 0
}

remove_base() {
    kubectl delete -f bootstrap
    return 0
}

deloy_ms() {
#Deloy services to kubernetes cluster
helm install authentification authentification/
helm install broker broker/
helm install listener listener/
helm install logger logger/
helm install mail mail/
sleep 5
helm install front front-end/
helm install prom prometheus/
return 0
}

remove_ms() {
helm uninstall prom
helm uninstall front-end
helm uninstall authentification
helm uninstall broker
helm uninstall listener
helm uninstall logger
helm uninstall mail
#Deloy services to kubernetes cluster
kubectl delete -f bootstrap
return 0
}

case "$1" in
deploy-all)  echo "Deploy All services" 
        install_base || exit 10
        deloy_ms || exit 10
        ;;
deloy)  echo "Deploy services" 
        deloy_ms || exit 10
        ;;
remove) echo "Remove services from k8s"
        remove_ms || exit 20
        ;;
redeploy) echo "Redeployig services"
        remove
        deloy
        ;;
bootstrap) echo "Bootstrap services"
           install_base || exit 10
            ;;
remove-all) echo "Remove all services from k8s"
        remove_ms || exit 20
        remove_base || exit 20
        ;;

*)      echo "Usage: deploy {deloy|remove|redeploy|deloy-all|bootstrap|remove-all}"
        exit 2
        ;;
esac
exit 0
