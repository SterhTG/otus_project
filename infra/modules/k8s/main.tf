resource "yandex_kubernetes_cluster" "zonal_cluster_main" {
  name        = var.k8s_name
  description = var.k8s_description
  network_id  = data.yandex_vpc_network.default.id # "enpm3j82hr16pfrck0v8"
  master {
    version = var.k8s_version 
    zonal {
      zone      = var.zone
      subnet_id = var.subnet_id
    }

    public_ip = true

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        start_time = "02:00"
        duration   = "2h"
      }
    }

    master_logging {
      enabled                    = false
      kube_apiserver_enabled     = true
      cluster_autoscaler_enabled = true
      events_enabled             = true
      audit_enabled              = false
    }
  }

  service_account_id      = data.yandex_iam_service_account.k8s_master.id
  node_service_account_id = data.yandex_iam_service_account.k8s_worker.id

  labels = {
    application = "test"
    environment = "dev"
  }

  release_channel         = "RAPID"
  network_policy_provider = "CALICO"

  provisioner "local-exec" {
    command = "yc managed-kubernetes cluster get-credentials ${var.k8s_name} --external --force"
  }

}

resource "yandex_kubernetes_node_group" "main_node_group" {
  cluster_id  = yandex_kubernetes_cluster.zonal_cluster_main.id
  name        = "work-node"
  description = "description"
  version     = var.k8s_version 

  labels = {
    "application" = "test"
    "environment" = "dev"
  }

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat        = true
      subnet_ids = [var.subnet_id]
    }

    resources {
      memory = 16
      cores  = 4
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = true
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = var.k8s_nodes_number
    }
  }

  allocation_policy {
    location {
      zone = var.zone
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "03:00"
      duration   = "3h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "23:00"
      duration   = "4h30m"
    }
  }
}
