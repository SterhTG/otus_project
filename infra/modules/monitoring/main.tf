resource "yandex_compute_instance" "monitoring" {
  name = var.host_name

  labels = {
    tags = "monitoring"
  }
  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = var.monitoring_disk_image
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.public_key_path)}"
    serial-port-enable = var.enable_serial
  }

  connection {
    type        = "ssh"
    host        = self.network_interface.0.nat_ip_address
    user        = "ubuntu"
    agent       = false
    private_key = file(var.private_key_path)
  }

  provisioner "file" {
    source      = "${path.module}/files/configs"
    destination = "/tmp"
  }

  provisioner "file" {
    source      = "${path.module}/files/install_sw.sh"
    destination = "/tmp/install_sw.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sleep 60",
      "sudo chmod +x /tmp/install_sw.sh",
      "sudo /tmp/install_sw.sh",
      "sudo rm /tmp/install_sw.sh",
    ]
  }

}
