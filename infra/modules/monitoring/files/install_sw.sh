#!/bin/bash

#create user and folders
sudo mkdir -p /etc/alertmanager
sudo mkdir -p /etc/prometheus
sudo mkdir -p /var/lib/alertmanager/data
sudo mkdir /var/lib/prometheus/
addgroup --system prometheus
adduser --system --no-create-home --shell /sbin/nologin --ingroup prometheus prometheus
#Install sw
sudo apt-get install -y adduser libfontconfig1 musl
wget https://dl.grafana.com/oss/release/grafana_11.0.0_amd64.deb
sudo dpkg -i grafana_11.0.0_amd64.deb
wget https://github.com/prometheus/prometheus/releases/download/v2.52.0/prometheus-2.52.0.linux-amd64.tar.gz
wget https://github.com/prometheus/alertmanager/releases/download/v0.27.0/alertmanager-0.27.0.linux-amd64.tar.gz
tar xfz prometheus-2.52.0.linux-amd64.tar.gz
tar xzf alertmanager-0.27.0.linux-amd64.tar.gz
cd prometheus-2.52.0.linux-amd64
sudo install -o root -g root -m 755 prometheus /usr/local/bin
sudo install -o root -g root -m 755 promtool /usr/local/bin
sudo cp -r console_libraries /etc/prometheus/
sudo cp -r consoles /etc/prometheus/
cp /tmp/configs/prometheus.service /etc/systemd/system/prometheus.service
cp /tmp/configs/config_prometheus.yml /etc/prometheus/prometheus.yml
cp /tmp/configs/alert_prometheus.yml /etc/prometheus/k8s_alerts.yml
cd ../alertmanager-0.27.0.linux-amd64/
sudo install -o root -g root -m 755 alertmanager /usr/local/bin
sudo install -o root -g root -m 755 amtool /usr/local/bin
cp /tmp/configs/alertmanager.service /etc/systemd/system/alertmanager.service
cp /tmp/configs/config_alertmanager.yml /etc/alertmanager/alertmanager.yml
cd ../
sudo chown -R prometheus:prometheus /var/lib/alertmanager /var/lib/prometheus
sudo chown -R root:prometheus /etc/prometheus /etc/alertmanager
#Cleanup
rm -Rf /tmp/configs ./prometheus-2.52.0.linux-amd64 ./alertmanager-0.27.0.linux-amd64 
rm prometheus-2.52.0.linux-amd64.tar.gz alertmanager-0.27.0.linux-amd64.tar.gz grafana_11.0.0_amd64.deb
#Activate and Enable services
systemctl daemon-reload
systemctl enable prometheus --now
systemctl enable alertmanager --now
systemctl enable grafana-server --now

