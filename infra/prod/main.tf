module "k8s" {
  source                   = "../modules/k8s"
  service_account_key_file = var.service_account_key_file
  public_key_path          = var.public_key_path
  private_key_path         = var.private_key_path
  subnet_id                = var.subnet_id
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  zone                     = "ru-central1-a"
  k8s_name                 = "project"
  k8s_nodes_number         = 2
  k8s_svc_account          = var.k8s_svc_account
  k8s_worker_svc_account   = var.k8s_worker_svc_account
}

module "monitoring" {
  source                   = "../modules/monitoring"
  service_account_key_file = var.service_account_key_file
  public_key_path          = var.public_key_path
  private_key_path         = var.private_key_path
  subnet_id                = var.subnet_id
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  zone                     = "ru-central1-a"
  monitoring_disk_image    = var.monitoring_disk_image
  host_name                = var.monitoring_host_name
  enable_serial            = 1
}
