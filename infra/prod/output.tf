output "monitoring_ip" {
  value = module.monitoring.external_ip_address_monitoring
  description = "The private IP address of the main server instance."
}
